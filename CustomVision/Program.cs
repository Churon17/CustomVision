﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.Cognitive.CustomVision;

namespace CustomVision.Sample
{
    class Program
    {
        private static List<MemoryStream> MbikesImages;

        private static List<MemoryStream> RbikesImages;

        private static MemoryStream testImage;

        static void Main(string[] args)
        {
            string trainingKey = GetTrainingKey("f9f83be0322245e8be9f65775c9475b0", args);

            TrainingApiCredentials trainingCredentials = new TrainingApiCredentials(trainingKey);
            TrainingApi trainingApi = new TrainingApi(trainingCredentials);

            Console.WriteLine("Creating new project:");
            var project = trainingApi.CreateProject("Bike Type");



            // Make two tags in the new project  
            var MbikesTag = trainingApi.CreateTag(project.Id, "Mountain");
            var RbikesTag = trainingApi.CreateTag(project.Id, "Racing");

            // Add some images to the tags  
            Console.WriteLine("\\tUploading images");
            LoadImagesFromDisk();

            // Images can be uploaded one at a time  
            foreach (var image in MbikesImages)
            {
                trainingApi.CreateImagesFromData(project.Id, image, new List<string>() { MbikesTag.Id.ToString() });
            }

            // Or uploaded in a single batch   
            trainingApi.CreateImagesFromData(project.Id, RbikesImages, new List<Guid>() { RbikesTag.Id });

            // Now there are images with tags start training the project
            Console.WriteLine("\\tTraining");
            var iteration = trainingApi.TrainProject(project.Id);

            // The returned iteration will be in progress, and can be queried periodically to see when it has completed  
            while (iteration.Status == "Training")
            {
                Thread.Sleep(1000);

                // Re-query the iteration to get it's updated status  
                iteration = trainingApi.GetIteration(project.Id, iteration.Id);
            }

            // The iteration is now trained. Make it the default project endpoint  
            iteration.IsDefault = true;
            trainingApi.UpdateIteration(project.Id, iteration.Id, iteration);
            Console.WriteLine("Done!\\n");

            // Now there is a trained endpoint, it can be used to make a prediction  

            // Get the prediction key, which is used in place of the training key when making predictions  
           
            var predictionKey = "5d4c493eea4945bb8ff38738a0c83309";

            // Create a prediction endpoint, passing in a prediction credentials object that contains the obtained prediction key  
            PredictionEndpointCredentials predictionEndpointCredentials = new PredictionEndpointCredentials(predictionKey);
            PredictionEndpoint endpoint = new PredictionEndpoint(predictionEndpointCredentials);

            // Make a prediction against the new project  
            Console.WriteLine("Making a prediction:");
            var result = endpoint.PredictImage(project.Id, testImage);

            // Loop over each prediction and write out the results  
            foreach (var c in result.Predictions)
            {
                Console.WriteLine($"\\t{c.Tag}: {c.Probability:P1}");
            }
            Console.ReadKey();




        }


        private static string GetTrainingKey(string trainingKey, string[] args)
        {
            if (string.IsNullOrWhiteSpace(trainingKey) )
            {
                if (args.Length >= 1)
                {
                    trainingKey = args[0];
                }

                while (string.IsNullOrWhiteSpace(trainingKey) || trainingKey.Length != 32)
                {
                    Console.Write("Enter your training key: ");
                    trainingKey = Console.ReadLine();
                }
                Console.WriteLine();
            }

            Console.WriteLine(trainingKey);

            return trainingKey;
        }

        private static void LoadImagesFromDisk()
        {
            // this loads the images to be uploaded from disk into memory
            MbikesImages = Directory.GetFiles(@"..\..\..\..\Images\Mountain").Select(f => new MemoryStream(File.ReadAllBytes(f))).ToList();
            RbikesImages = Directory.GetFiles(@"..\..\..\..\Images\Racing").Select(f => new MemoryStream(File.ReadAllBytes(f))).ToList();
            testImage = new MemoryStream(File.ReadAllBytes(@"..\..\..\..\Images\test\bike1.jpg"));

        }
    }
}
